**PROJECT : Othello**

Othello is a game played by two people on an 8 x 8 board, using disks that are white on one side and black on the other. One player places disks with the white side up and the other player places disks with the black side up. The players alternate placing one disk on an unoccupied space on the board. 

**TEAM MEMBERS**
- AKHILA : 19WH1A0244 : EEE
- PRAVANI:19WH1A05A6 : CSE 
- SRAVANI: 19WH1A0573 : CSE   
- SELVI REDDY:19WH1A1215 : IT 
- MANASVI:20WH5A1203 : IT
